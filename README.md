
# Chatterm

Chatterm is a command-line interface (CLI) tool for interacting with OpenAI's GPT models. It offers a convenient way to engage in conversations, execute commands, and utilize various functionalities right from your terminal.

## Features:

- **Simple Interaction**: Just type your message and get a response.
- **Command Execution**: Use ! as a prefix to execute terminal commands and pass the output to GPT.
- **Model Reselection**: Easily switch between different GPT models for varied responses.
- **PDF Content Extraction**: Extract and send text from PDF files to ChatGPT for specific queries
- **Token Usage Tracking**: Keep track of API token usage for effective quota management.

## Installation:

1. Ensure you have Python 3 installed.
2. Install required Python packages: `pip install openai pdfminer.six termcolor`.
3. Clone this repository or download `chatterm.py` and `spinner.py`.
4. Make `chatterm.py` executable: `chmod +x chatterm.py`.

## Usage:

1. **First-Time Setup**: Run the script with the `--setup` flag to set up your OpenAI API key and choose a model. `gpt-3.5-turbo-16k-0613` is recommended.

```bash
./chatterm.py --setup
```
2. **Simple Chat**: Just type your message after the script name.

```bash
./chatterm.py "Tell me a joke."
```

3. **Command Execution**: To execute a terminal command and pass its output to GPT-3, prefix your message with `!`.

```bash
./chatterm.py "! ls"
```

Or for more complex commands:

```bash
./chatterm.py "! sudo dmesg | grep 'fail'"
```

4. **Reselect Model**: If you wish to change the GPT-3 model you're using, you can do so with the `--reselect` flag.

```bash
./chatterm.py --reselect
```

5. **Clear Chat History**: If you want to clear the chat history and start afresh, use the `--clear` flag.

```bash
./chatterm.py --clear
```

6. **View Token Usage**: To view your total token usage, use the `--token-usage` flag.

```bash
./chatterm.py --token-usage
```

7. **View Token Usage**: To clear your token usage, use the `--clear-token` flag.

```bash
./chatterm.py --clear-token
```

7. **Get Version and Model Info**: You can view the version, copyright, and currently selected model using the `--version` flag.

```bash
./chatterm.py --version
```
8. **PDF Content Extraction with Message**:

To send the content of a PDF file to ChatGPT and request a summary:

```bash
./chatterm.py loremipsum.pdf "give me a summary"
```



## Note:

The continuous chat feature depends on the model's token limit. If the conversation becomes too lengthy and exceeds the model's token limit, you might need to clear the chat history or make your inputs shorter.

## Dislclaimer
This tool serves as a CLI interface for communication with ChatGPT by OpenAI. Please be aware of the following:

1. **Data Usage**: The author of this tool does not have insight into how OpenAI handles or processes the data sent to ChatGPT. Before using this tool, users should familiarize themselves with OpenAI's data usage policies and terms of service.

2. **No Responsibility**: The author of this tool is not responsible for any outcomes, interpretations, or actions resulting from the use of this tool or the information provided by ChatGPT.

3. **Use with Caution**: As with any online service, users should exercise caution and discretion when sharing sensitive or personal information.

## License:

Copyright A.Warth 2023 with the support of ChatGPT 

