#!/usr/bin/env python3
import sys
from spinner import Spinner
from pdfminer.high_level import extract_text

from openai import OpenAI

import argparse
import os
import json
import subprocess
import re
from termcolor import colored
from spinner import Spinner

VERSION = "1.2.0"
COPYRIGHT = "2023, Alexander Warth"

CONFIG_PATH = os.path.expanduser("~/.chatgpt_config")
CHAT_HISTORY_PATH = os.path.expanduser("~/.chatgpt_history")
TOKEN_USAGE_PATH = os.path.expanduser("~/.chatgpt_token_usage")


chat_history = []

def save_config(api_key, model):
    try:
        # Test the API key by listing available models
        client = OpenAI(api_key=api_key)
        client.models.list()

        config = {"api_key": api_key, "model": model}
        with open(CONFIG_PATH, 'w') as f:
            json.dump(config, f)
        os.chmod(CONFIG_PATH, 0o600)
        print("Configuration saved successfully.")
    except Exception as e:
        print(f"Failed to save configuration: {e}")


def get_config():
    # Try-except block to handle potential file not found error
    try:
        # Attempt to open the configuration file in read mode
        with open(CONFIG_PATH, 'r') as f:
            # Load and return the JSON content of the file
            # This content is expected to be a dictionary containing the API key and model
            return json.load(f)
    except FileNotFoundError:
        # This block executes if the configuration file does not exist
        # A message is printed to inform the user that the configuration file is missing
        print("Configuration not found. Please run the tool with --setup to configure.")
        # The script is exited with a non-zero status (1), indicating an error
        exit(1)

def save_chat_history():
    # Open the chat history file in write mode
    with open(CHAT_HISTORY_PATH, 'w') as f:
        # Write the contents of the chat_history list to the file in JSON format
        # The chat_history list is assumed to contain the history of the chat sessions
        json.dump(chat_history, f)

    # Call the get_total_tokens function
    # This function is not defined in the snippets you've provided,
    # but it likely calculates and possibly updates the total number of tokens used
    # in the chat sessions, which is an important aspect for tracking API usage
    get_total_tokens()

    # Change the file permissions of the chat history file to 600
    # Similar to the save_config function, this sets the file to be readable and writable
    # only by the file's owner, which is a security measure to protect the chat history
    os.chmod(CHAT_HISTORY_PATH, 0o600)

def load_chat_history():
    global chat_history  # Declare chat_history as a global variable to modify it

    # Try-except block to handle potential file not found error
    try:
        # Attempt to open the chat history file in read mode
        with open(CHAT_HISTORY_PATH, 'r') as f:
            # Load the JSON content of the file into the chat_history variable
            chat_history = json.load(f)
    except FileNotFoundError:
        # If the file is not found, initialize chat_history as an empty list
        chat_history = []

def save_token_usage(tokens):
    """
    Persist the total token usage to a file for future reference.
    """
    with open(TOKEN_USAGE_PATH, 'w') as f:
        json.dump({"tokens_used": tokens}, f)  # Write the token count as JSON
    os.chmod(TOKEN_USAGE_PATH, 0o600)  # Secure the file by restricting access


def load_token_usage():
    """
    Load the total token usage from a file. If the file doesn't exist, assume no tokens have been used.
    """
    try:
        with open(TOKEN_USAGE_PATH, 'r') as f:
            data = json.load(f)  # Read the token count from the file
        return data.get("tokens_used", 0)  # Return the token count or 0 if not previously set
    except FileNotFoundError:
        return 0  # Return 0 if the token usage file does not exist

def reset_tokens():
    # This function attempts to reset the token usage count to zero
    try:
        save_token_usage(0)  # Set the token count to zero by calling save_token_usage with 0
        print("Token count has been set to zero.")  # Inform the user that the token count has been reset
    except Exception as e:
        # If an exception occurs during the process, print an error message
        print(f"An error occurred while setting the token count to zero: {e}")

def chat_with_gpt(client, model, user_message):
    global chat_history  # Access the global chat history variable

    # Append the user message to the chat history for context
    chat_history.append({"role": "user", "content": user_message})

    try:
        # Send the chat history to the API to maintain context in the conversation
        response = client.chat.completions.create(model=model, messages=chat_history)

        # Extract the assistant's response from the API response
        assistant_message = response.choices[0].message.content
        
        # Retrieve the actual token usage from the API response
        tokens_used = response.usage.total_tokens
        
        # Update the cumulative token usage
        update_token_usage(tokens_used)
        
        # Calculate the total tokens used so far for display
        total_tokens = load_token_usage()

        # Format the assistant's response with token usage information, coloring the token info in cyan
        token_info = colored(f"Tokens used: {tokens_used}\nTotal tokens used: {total_tokens}", 'cyan')
        formatted_response = f"{assistant_message}\n\n{token_info}"

        # Append the assistant's response to the chat history
        # Note: We add only the message content to the chat history to exclude token usage info
        chat_history.append({"role": "assistant", "content": assistant_message})
        
        # Persist the updated chat history
        save_chat_history()

    except openai.error.OpenAIError as e:
        # Handle any errors that occur during the API request
        formatted_response = colored(f"An error occurred: {str(e)}", 'red')

    # Return the formatted response including token usage, not altering chat history
    return formatted_response

def update_token_usage(tokens_used):
    """
    Update the token usage file or variable with the number of tokens used in the latest API call.
    """
    current_usage = load_token_usage()  # Load the current total token usage
    new_total = current_usage + tokens_used  # Add the latest token usage to the total
    save_token_usage(new_total)  # Save the updated total usage

def get_total_tokens():
    """
    Retrieve the total token usage directly from the stored data.
    This approach uses the cumulative token usage data, which should be updated
    after each interaction with the OpenAI API, to reflect accurate token consumption.
    """
    # Load the total token usage from the stored data
    total_tokens = load_token_usage()
    
    # Simply return the total token count
    return total_tokens

def execute_command(command):
    """Execute a shell command and return its output."""
    try:
        # Run the specified command in the shell and capture its output
        # stderr is redirected to stdout, so errors are captured as well
        result_bytes = subprocess.check_output(command, shell=True, stderr=subprocess.STDOUT)
        # Decode the byte output to a string, replacing any decoding errors
        result = result_bytes.decode('utf-8', 'replace')
    except subprocess.CalledProcessError as e:
        # If the command execution fails, catch the exception
        # e.output contains the output of the failed command, including error messages
        result = e.output.decode('utf-8', 'replace') if e.output else "Error executing command"
    return result


def choose_model(api_key):
    client = OpenAI(api_key=api_key)
    try:
        available_models = client.models.list()
        
        # Regex pattern to match models suitable for chat completions
        pattern = r"gpt-3\.5-turbo|gpt-4(-turbo)?"
        
        # Filter available models based on the regex pattern
        model_names = sorted([
            model.id for model in available_models.data
            if re.search(pattern, model.id, re.IGNORECASE)
        ])
        
        if not model_names:
            print("No supported models available for chat completions.")
            return None
        
        print("Available models for chat completions:")
        for i, model_name in enumerate(model_names, start=1):
            print(f"{i}. {model_name}")
        
        choice = -1
        while choice not in range(1, len(model_names) + 1):
            try:
                choice = int(input("Choose a model by entering its number: "))
            except ValueError:
                print("Invalid input. Please enter a number corresponding to the model.")
        
        return model_names[choice - 1]
    except Exception as e:
        print(f"Error fetching models: {e}")
        return None

def reselect_model():
    # Retrieve the current configuration, including the API key
    config = get_config()
    api_key = config["api_key"]

    # Call the choose_model function to allow the user to select a new model
    model = choose_model(api_key)

    # Save the new model selection along with the existing API key in the configuration
    save_config(api_key, model)

    # Inform the user that the model has been updated
    print(f"Model updated to {model} in {CONFIG_PATH}")

def read_pdf(file_path):
    # Initialize a spinner to indicate progress while extracting text from a PDF
    spinner = Spinner("Extracting text from PDF...")
    spinner.start()  # Start the spinner

    # Use the extract_text function from the pdfminer library to extract text from the PDF file
    text = extract_text(file_path)

    spinner.stop()  # Stop the spinner once the text extraction is completed

    # Return the extracted text from the PDF
    return text

def main():
    # Load the chat history at the start
    load_chat_history()

    # Set up command-line argument parsing
    parser = argparse.ArgumentParser(description="CLI for ChatGPT using OpenAI API")
    parser.add_argument("message", type=str, nargs='?', default='', help="Message to send to ChatGPT")
    parser.add_argument("inputs", type=str, nargs="*", help="Inputs to the script (e.g., PDF file and message)")
    parser.add_argument("--setup", action='store_true', help="Setup the API key and model")
    parser.add_argument("--reselect", action='store_true', help="Reselect the model")
    parser.add_argument("--version", action='store_true', help="Display version, copyright, and the currently selected model")
    parser.add_argument("--clear", action='store_true', help="Clear chat history")
    parser.add_argument("--clear-token", action='store_true', help="Clear token counter")
    parser.add_argument("--token-usage", action='store_true', help="Display the total token usage")
    parser.add_argument("--pdf", help="Path to the PDF file to be read by ChatGPT.", type=str)
    args = parser.parse_args()

    # Handling the case when a PDF file is provided as the first command-line argument
    if len(sys.argv) > 2 and sys.argv[1].endswith('.pdf'):
        pdf_path = sys.argv[1]
        additional_message = sys.argv[2]
        pdf_content = read_pdf(pdf_path)
        args.message = f"Here is the content of the document: {pdf_content}. {additional_message}"

    # Handling the case when a PDF file is provided using the --pdf flag
    if args.pdf:
        pdf_content = read_pdf(args.pdf)
        args.message = pdf_content

    # Handling various command-line flags
    if args.clear:
        if os.path.exists(CHAT_HISTORY_PATH):
            os.remove(CHAT_HISTORY_PATH)
        chat_history.clear()
        print("Chat history cleared.")
        return

    if args.clear_token:
        if os.path.exists(TOKEN_USAGE_PATH):
            reset_tokens()
        print("Total tokens cleared")
        return

    if args.token_usage:
        total_tokens = get_total_tokens()
        print(f"Total tokens used: {total_tokens}")
        return

    if args.setup:
        api_key = input("Enter your OpenAI API key: ")
        model = choose_model(api_key)
        save_config(api_key, model)
        print(f"Configuration saved in {CONFIG_PATH}")
        return

    # Check for the existence of the configuration file
    if not os.path.exists(CONFIG_PATH):
        print("Configuration not found. Please run the tool with --setup first.")
        return

    config = get_config()

    # Check if the configuration is complete
    if "api_key" not in config or "model" not in config:
        print("Incomplete configuration. Please run the tool with --setup to reconfigure.")
        return

    # Handle reselecting the model
    if args.reselect:
        reselect_model()
        return

    # Handle version information display
    if args.version:
        print(f"ccli.py version: {VERSION}")
        print(f"Copyright: {COPYRIGHT}")
        print(f"Currently selected model: {config['model']}")
        return

    # Create an OpenAI client using the API key from the configuration
    client = OpenAI(api_key=config["api_key"])
    model = config["model"]

    user_input = args.message

    # Check if the input is a command to be executed in the shell
    if user_input.startswith('!'):
        command = user_input[1:].strip()
        print(f"Executed Command:")
        print(colored(f"{command}",'cyan'))
        command_output = execute_command(command)
        print(f"Command Output:")
        print(colored(f"{command_output}",'magenta'))  # Displaying the output of the command
        user_input = f"I executed the command `{command}` in the terminal and got:\n{command_output}"

    # Start a spinner to indicate communication with ChatGPT
    spinner = Spinner("Communicating with ChatGPT...")
    spinner.start()

    # Try to get a response from ChatGPT
    try:
        response_text = chat_with_gpt(client, model, user_input)
        # Stop the spinner once the response is obtained
        spinner.stop()
        # Print the response from ChatGPT
        print(colored(f"ChatGPT:\n{response_text}", 'green'))

    # Handle API rate limit errors
    except openai.error.RateLimitError:
        spinner.stop()
        print("You've exceeded your OpenAI API quota. Please try again later or check your plan and billing details.")
    # Handle bad request errors, likely due to an invalid model selection
    except openai.error.BadRequestError as e:
        spinner.stop()
        print("Detailed Error:", str(e))
        print("Error: The specified model does not exist or you do not have access to it.")
        print("You may want to reselect a different model using the --reselect flag.")

if __name__ == "__main__":
    main()  # Run the main function if the script is executed as a standalone program
