import sys
import time
import threading
import itertools

class Spinner:
    def __init__(self, message="Working..."):
        # Initialize a spinner animation using a cycle of Unicode characters
        # This cycle creates a spinning effect in the console
        self.spinner_cycle = itertools.cycle(["⠋", "⠙", "⠹", "⠸", "⠼", "⠴", "⠦", "⠧", "⠇", "⠏"])

        # Boolean to track if the spinner should be running
        self.running = False

        # Create a separate thread for the spinner animation
        self.spinner_thread = threading.Thread(target=self.initiate_spin)

        # Store the message to be displayed alongside the spinner
        self.message = message

    def start(self):
        # Start the spinner
        self.running = True

        # Display the first frame of the spinner animation and the message
        sys.stdout.write(f"\r{next(self.spinner_cycle)} {self.message} ")
        sys.stdout.flush()

        # Start the spinner thread
        self.spinner_thread.start()

    def stop(self):
        # Stop the spinner
        self.running = False

        # Wait for the spinner thread to finish
        self.spinner_thread.join()

        # Display a completion message with a checkmark
        sys.stdout.write(f"\r[[92m✓[0m] " + self.message + "\n")
        sys.stdout.flush()

    def initiate_spin(self):
        # Function that runs in the separate thread to animate the spinner
        while self.running:
            # Update the spinner frame and message, then flush to ensure it displays
            sys.stdout.write(f"\r{next(self.spinner_cycle)} {self.message} ")
            sys.stdout.flush()

            # Pause briefly before displaying the next frame
            time.sleep(0.1)
